#!/bin/bash

generateToken() {
    randomKey=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 24 | head -n 1`
    echo "$randomKey"
}

build() {
    token=$1
    name=$2    
    mkdir -p out
    cd aws-iot-ethernet.X
    make clean
    make CFLAGS=-DTOKEN1=$token
    
#    mv dist/IoT_Ethernet/production/{aws-iot-ethernet.X.production.hex,$name.hex}
    mv dist/IoT_Ethernet/production/aws-iot-ethernet.X.production.hex ../out/$name.hex

    echo "Hexfile '$name' generated at 'out'"
}

token=$( generateToken )
name="Generic-IoT-Hex-$token"
echo "Building Generic IoT firmware with token : " + $token
build $token $name

