/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portevent.c,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */

/* ----------------------- Modbus includes ----------------------------------*/
#include "../../src/modbus/include/mb.h"
#include "../../src/modbus/include/mbport.h"

/* ----------------------- Variables ----------------------------------------*/
static eMBEventType eQueuedEvent;
static uint8_t     xEventInQueue;

/* ----------------------- Start implementation -----------------------------*/
uint8_t
xMBPortEventInit( void )
{
    xEventInQueue = kFALSE;
    return kTRUE;
}

uint8_t
xMBPortEventPost( eMBEventType eEvent )
{
    xEventInQueue = kTRUE;
    eQueuedEvent = eEvent;
    return kTRUE;
}

uint8_t
xMBPortEventGet( eMBEventType * eEvent )
{
    uint8_t            xEventHappened = kFALSE;

    if( xEventInQueue )
    {
        *eEvent = eQueuedEvent;
        xEventInQueue = kFALSE;
        xEventHappened = kTRUE;
    }
    return xEventHappened;
}
